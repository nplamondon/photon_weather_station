#include "DHT.h"

// Example testing sketch for various DHT humidity/temperature sensors
// Written by ladyada, public domain
// Modified by Nate Plamondon

#define DHTPIN 2     // what pin we're connected to

// Uncomment whatever type you're using!
#define DHTTYPE DHT11           // DHT 11
//#define DHTTYPE DHT22         // DHT 22 (AM2302)
//#define DHTTYPE DHT21         // DHT 21 (AM2301)

// Name of this node
#define LOCATION "Nate_home"

// Uncomment the system you want.
#define SCALE 'C'
//#define SCALE 'F'
//#define SCALE 'K'

// Connect pin 1 (on the left) of the sensor to +5V
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

DHT dht(DHTPIN, DHTTYPE);

void setup() {
        Serial.begin(9600);
        Serial.println("DHTxx test!");

        dht.begin();
}

void loop() {
// Wait a few seconds between measurements.
        delay(2000);

// Reading temperature or humidity takes about 250 milliseconds!
// Sensor readings may also be up to 2 seconds 'old' (its a
// very slow sensor)
        float h = dht.getHumidity();
// Read temperature as Celsius
        float t = dht.getTempCelcius();
        float f = dht.convertCtoF(t);

// Check if any reads failed and exit early (to try again).
        if (isnan(h) || isnan(t) || isnan(f)) {
                Serial.println("Failed to read from DHT sensor!");
                return;
        }

// Compute heat index
// Must send in temp in Fahrenheit!
        float hi = dht.computeHeatIndex(f, h); // output in F
        float dp = dht.computeDewPoint(t, h);

// convert to desired scale
    float tOut;
    float hiOut;
    float dpOut;

    switch (SCALE) {
        case 'F':
            tOut = dht.convertCtoF(t);
            hiOut = hi;
            dpOut = dht.convertCtoF(dp);
            break;
        case 'K':
            tOut = dht.convertCtoK(t);
            hiOut = dht.convertCtoK(dht.convertFtoC(hi));
            dpOut = dht.convertCtoK(dp);
            break;
        default: // default to C
            tOut = t;
            hiOut = dht.convertFtoC(hi);
            dpOut = dp;
    }

    String out = "loc=" + String(LOCATION) + " scale=" + String(SCALE) + " temperature=" + tOut + " humidity=" + h + \
                     " dew_point=" + dpOut + " heat_index=" + hiOut;
    Particle.publish("Environment report", out);
}
